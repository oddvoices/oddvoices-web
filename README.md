## OddVoices Web

OddVoices Web is a Web frontend for the OddVoices singing synthesizer, made by compiling the project to WASM. Upload a MIDI file and some English text and OddVoices will sing it for you.

OddVoices Web is written in TypeScript with some C++ glue code and uses Emscripten and webpack.

### Setup

Ensure all submodules are checked out:

    git submodule update --init --recursive

Install and activate [emsdk](https://emscripten.org/docs/getting_started/downloads.html), then install npm dependencies:

    npm install

To build:

    npm run build

Then serve the directory `app`, e.g.:

    python3 -m http.server -d app

and visit localhost:8000 in your browser.
